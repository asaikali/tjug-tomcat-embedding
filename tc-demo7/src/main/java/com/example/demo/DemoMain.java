package com.example.demo;

import java.io.File;

import org.apache.catalina.Globals;
import org.apache.catalina.startup.Catalina;

/**
 * 
 * Sorry by this demo is not complete!
 *
 */
public class DemoMain {
	public static void main(String[] args) {
		String home = new File("tomcat").getAbsolutePath();
		System.setProperty(Globals.CATALINA_HOME_PROP, home);
		System.setProperty(Globals.CATALINA_BASE_PROP, home);
		Catalina catalina = new Catalina();
		catalina.start();
	}
}
